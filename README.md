# hello with dfinity

## Prerequisites

- [dfinity](https://dfinity.org/developers)
- [rust](https://www.rust-lang.org/)

## Run & Dev

Start

```sh
dfx start --background
```

Deploy

```sh
dfx deploy
```

Test

```sh
dfx canister call hello print
```

Stop

```sh
dfx stop
```